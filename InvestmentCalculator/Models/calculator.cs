﻿namespace InvestmentCalculator.Models
{
    public class calculator
    {
        public double EndAmount { get; set; }

        public double StartAmount { get; set; }

        public double MonthlyContribution { get; set; }

        public double ReturnRate { get; set; }

        //public int NumofTimesInterestCompund { get; set;}

        public double NumofInvestmentYears { get; set; }

    }
}