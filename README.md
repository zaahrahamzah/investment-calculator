# Investment Calculator

This is an Investment Calculator.

This application was built using ASP.NET MVC 5 web framework. It serves two (2) main function. 

1. To calculate the End Amount of the investment given that the user provided the start amount, monthly contribution, return rate and the number of investment year. 

2. To calculate the Investment Length needed to achieve the end amount (goal) of the investment, given that the user provided the start amount, monthly contribution, return rate and end amount.
